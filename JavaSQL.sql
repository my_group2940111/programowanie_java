CREATE SCHEMA IF NOT EXISTS `library_database`;
CREATE TABLE IF NOT EXISTS `library_database`.`authors` (
  `id_authors` INT NOT NULL,
  `name` VARCHAR(20) NOT NULL,
  `last_name` VARCHAR(20) NOT NULL,
  `date_of_birth` DATE NOT NULL,
  `country` VARCHAR(35) NOT NULL,
  PRIMARY KEY (`id_authors`))
ENGINE = InnoDB;
CREATE TABLE IF NOT EXISTS `library_database`.`books` (
  `id_books` INT NOT NULL,
  `title` VARCHAR(300) NOT NULL,
  `publishing_house` VARCHAR(45) NOT NULL,
  `date_of_publishing` DATE NOT NULL,
  `isbn` VARCHAR(13) NOT NULL,
  `price` FLOAT NOT NULL,
  `serie` VARCHAR(50) NULL,
  `serie_volume` INT NULL,
  PRIMARY KEY (`id_books`))
ENGINE = InnoDB
CREATE TABLE IF NOT EXISTS `library_database`.`authors_books` (
  `id_authors_books` INT NOT NULL,
  `id_authors` INT NOT NULL,
  `id_books` INT NOT NULL,
  `spinner_count` INT NOT NULL,
  PRIMARY KEY (`id_authors_books`),
  INDEX `idAuthors_idx` (`id_authors` ASC) VISIBLE,
  INDEX `idBooks_idx` (`id_books` ASC) VISIBLE,
  CONSTRAINT `id_authors`
    FOREIGN KEY (`id_authors`)
    REFERENCES `library_database`.`authors` (`id_authors`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `id_books`
    FOREIGN KEY (`id_books`)
    REFERENCES `library_database`.`books` (`id_books`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB



INSERT INTO `library_database`.`books` (`id_books`, `title`, `publishing_house`, `date_of_publishing`, `isbn`, `price`, `serie`) VALUES ('1', 'Opowiem o tobie gwizadom', 'Moondrive', '2022-07-13', '9788380111417', '44.99', '');
INSERT INTO `library_database`.`books` (`id_books`, `title`, `publishing_house`, `date_of_publishing`, `isbn`, `price`) VALUES ('2', 'Jak powstaje morderca. Zagadki psychopatycznych umysłów', 'Wydawnictwo Feeria Science', '2022-04-27', '9788382251692', '52.90');
INSERT INTO `library_database`.`books` (`id_books`, `title`, `publishing_house`, `date_of_publishing`, `isbn`, `price`) VALUES ('3', 'Jak mniej myśleć? Dla analizujących bez koñca i wysoko wrażliwych', 'Wydawnictwo Feeria', '2019-04-24', '9788372298393', '44.90');
INSERT INTO `library_database`.`books` (`id_books`, `title`, `publishing_house`, `date_of_publishing`, `isbn`, `price`) VALUES ('4', 'It Ends with Us', 'Wydawnictwo Otwarte', '2021-03-17', '97815011103683', '33.63');


INSERT INTO `library_database`.`authors` (`id_authors`, `name`, `last_name`, `date_of_birth`, `country`) VALUES ('1', 'Edyta', 'Prusinowska', '2000-09-17', 'Polska');
INSERT INTO `library_database`.`authors` (`id_authors`, `name`, `last_name`, `date_of_birth`, `country`) VALUES ('2', 'John', 'Douglas', '1944-01-01', 'Stany Zjednoczone');
INSERT INTO `library_database`.`authors` (`id_authors`, `name`, `last_name`, `date_of_birth`, `country`) VALUES ('3', 'Mark', 'Olshaker', '1951-02-22', 'Stany Zjednoczone');
INSERT INTO `library_database`.`authors` (`id_authors`, `name`, `last_name`, `date_of_birth`, `country`) VALUES ('4', 'Christel', 'Petitcollin', '1978-04-21', 'Brazylia');
INSERT INTO `library_database`.`authors` (`id_authors`, `name`, `last_name`, `date_of_birth`, `country`) VALUES ('5', 'Coolleen', 'Hoover', '1979-12-11', 'Stany Zjednoczone');

INSERT INTO `library_database`.`authors_books`(`id_authors_books`, `id_authors`, `id_books`, `spinner_count`) VALUES('1', '1', '1', '1');
INSERT INTO `library_database`.`authors_books`(`id_authors_books`, `id_authors`, `id_books`, `spinner_count`) VALUES('2', '2', '2', '1');
INSERT INTO `library_database`.`authors_books`(`id_authors_books`, `id_authors`, `id_books`, `spinner_count`) VALUES('3', '3', '2', '2');
INSERT INTO `library_database`.`authors_books`(`id_authors_books`, `id_authors`, `id_books`, `spinner_count`) VALUES('4', '4', '3', '1');
INSERT INTO `library_database`.`authors_books`(`id_authors_books`, `id_authors`, `id_books`, `spinner_count`) VALUES('5', '5', '4', '1');


