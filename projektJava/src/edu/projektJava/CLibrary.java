package edu.projektJava;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CLibrary{

    public static void insertBook(CBooks book) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = ConnectionConfiguration.getConnection();
            preparedStatement = connection.prepareStatement("INSERT INTO books " +
                    "(id_books, title, publishing_house, date_of_publishing, isbn, price)" +
                    "VALUES (?, ?, ?, ?, ?,?)");
            preparedStatement.setInt(1, book.getId_books());
            preparedStatement.setString(2, book.getTitle());
            preparedStatement.setString(3, book.getPublishingHouse());
            preparedStatement.setDate(4, Date.valueOf(book.getDateOfPublishing()));
            preparedStatement.setString(5,book.getIsbn());
            preparedStatement.setDouble(6, book.getPrice());
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void insertBookWithSerie(CBooks book) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = ConnectionConfiguration.getConnection();
            preparedStatement = connection.prepareStatement("INSERT INTO books " +
                    "(id_books, title, publishing_house, date_of_publishing, isbn, price, serie, serie_volume)" +
                    "VALUES (?, ?, ?, ?, ?, ?, ?,?)");
            preparedStatement.setInt(1,book.getId_books());
            preparedStatement.setString(2, book.getTitle());
            preparedStatement.setString(3, book.getPublishingHouse());
            preparedStatement.setDate(4, Date.valueOf(book.getDateOfPublishing()));
            preparedStatement.setString(5, book.getIsbn());
            preparedStatement.setDouble(6, book.getPrice());
            preparedStatement.setString(7,book.getSerie());
            preparedStatement.setInt(8,book.getSerieVolume());
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    public static CBooks selectBookById(int id) {
        CBooks book = new CBooks();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            connection = ConnectionConfiguration.getConnection();
            preparedStatement = connection.prepareStatement("SELECT * FROM books WHERE id_books = ?");
            preparedStatement.setInt(1, id);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                book.setId_books(resultSet.getInt("id_books"));
                book.setTitle(resultSet.getString("title"));
                book.setPublishingHouse(resultSet.getString("publishing_house"));
                book.setDateOfPublishing(resultSet.getString("date_of_publishing"));
                book.setIsbn(resultSet.getString("isbn"));
                book.setSerie(resultSet.getString("serie"));
                book.setSerieVolume(resultSet.getInt("serie_volume"));

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return book;
    }

    public static Boolean getId(String id){
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = ConnectionConfiguration.getConnection();
            String query = "SELECT id_books FROM books WHERE id_books = ?";
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, id);
            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

    public static int getLastId(String tableName, String id) {
        Connection connection = null;
        Statement statement = null;
        try {
            String query = "SELECT MAX( " + id + " ) + 1 FROM " + tableName;
            connection = ConnectionConfiguration.getConnection();
            statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(query);
            if (rs.next()) {
                return rs.getInt(1);
            }
        }catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return 1;
    }

    public static Boolean deleteBook(int id){
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try{
            connection = ConnectionConfiguration.getConnection();
            preparedStatement = connection.prepareStatement("DELETE FROM books WHERE id_books = ?");
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

    public static Boolean updateBook(CBooks book, int id) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = ConnectionConfiguration.getConnection();
            preparedStatement = connection.prepareStatement("UPDATE books SET title = ?, publishing_house = ?,  date_of_publishing = ?, isbn = ?, price = ?, serie = ?, serie_volume = ? WHERE id_books = ?");

            preparedStatement.setString(1, book.getTitle());
            preparedStatement.setString(2, book.getPublishingHouse());
            preparedStatement.setDate(3, Date.valueOf(book.getDateOfPublishing()));
            preparedStatement.setString(4, book.getIsbn());
            preparedStatement.setDouble(5,book.getPrice());
            preparedStatement.setString(6, book.getSerie());
            preparedStatement.setInt(7, book.getSerieVolume());
            preparedStatement.setInt(8, id);
            preparedStatement.executeUpdate();
            return true;

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

    public static void insertAuthor(CAuthors author) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = ConnectionConfiguration.getConnection();
            preparedStatement = connection.prepareStatement("INSERT INTO authors" +
                    "(id_authors, name, last_name, date_of_birth, country)" +
                    "VALUES (?, ?, ?, ?, ?)");
            preparedStatement.setInt(1, author.getIdAuthors());
            preparedStatement.setString(2, author.getName());
            preparedStatement.setString(3, author.getLastName());
            preparedStatement.setDate(4, Date.valueOf(author.getDateOfBirth()));
            preparedStatement.setString(5, author.getCountry());
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }



    public ResultSet searchBooks(String title, String publishingHouse, String series, String minDateBook, String maxDateBook, String code, String minPrice, String maxPrice, String serieVolume) throws SQLException {
        List<String> conditions = new ArrayList<>();
        List<Object> parameters = new ArrayList<>();
        String query = "SELECT * FROM BOOKS ";

        if(!title.isEmpty()){
            conditions.add("title like ?");
            parameters.add("%"+title+"%");
        }
        if(!publishingHouse.isEmpty()){
            conditions.add("publishing_house like ?");
            parameters.add("%"+publishingHouse+"%");
        }
        if(!series.isEmpty()){
            conditions.add("serie like ?");
            parameters.add("%"+series+"%");

        }
        if(!minDateBook.isEmpty()){
            conditions.add("date_of_publishing >= ?");
            parameters.add(minDateBook);
        }
        if(!maxDateBook.isEmpty()){
            conditions.add("date_of_publishing <= ?");
            parameters.add(maxDateBook);
        }
        if(!code.isEmpty()){
            conditions.add("isbn like ?");
            parameters.add(code);
        }
        if(!minPrice.isEmpty()){
            conditions.add("price >= ?");
            parameters.add(minPrice);
        }
        if(!maxPrice.isEmpty()){
            conditions.add("price <= ?");
            parameters.add(maxPrice);
        }
        if(!serieVolume.isEmpty()){
            conditions.add("serie_volume = ?");
            parameters.add(serieVolume);
        }

        if(!conditions.isEmpty()){
            query += "WHERE " + String.join(" AND ", conditions);

        }
        Connection connection = ConnectionConfiguration.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(query);
                 for(int i = 0; i < parameters.size(); i++){
            preparedStatement.setObject(i + 1, parameters.get(i));

        }
        return preparedStatement.executeQuery();
    }


    public ResultSet searchAuthors(String name, String surname, String minDateAuthor, String maxDateAuthor, String country) throws SQLException {
        List<String> conditions = new ArrayList<>();
        List<Object> parameters = new ArrayList<>();
        String query = "SELECT * FROM authors ";

        if(!name.isEmpty()){
            conditions.add("name like ?");
            parameters.add("%"+name+"%");
        }
        if(!surname.isEmpty()){
            conditions.add("last_name like ?");
            parameters.add("%"+surname+"%");
        }
        if(!minDateAuthor.isEmpty()){
            conditions.add("date_of_birth >= ?");
            parameters.add(minDateAuthor);
        }
        if(!maxDateAuthor.isEmpty()){
            conditions.add("date_of_brith <= ?");
            parameters.add(maxDateAuthor);
        }
        if(!country.isEmpty()){
            conditions.add("country like ?");
            parameters.add("%"+country+"%");
        }

        if(!conditions.isEmpty()){
            query += "WHERE " + String.join(" AND ", conditions);
        }
        Connection connection = ConnectionConfiguration.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        for(int i = 0; i < parameters.size(); i++){
            preparedStatement.setObject(i + 1, parameters.get(i));
        }
        return preparedStatement.executeQuery();
    }

    public static ResultSet selectAllBooks() throws SQLException {
        String query = "SELECT * FROM BOOKS ";

        Connection connection = ConnectionConfiguration.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(query);

        return preparedStatement.executeQuery();
    }


    public static StringBuilder selectBooksForAuthors(Integer authorID) throws SQLException{

        String query = "SELECT DISTINCT books.title " +
                "FROM books " +
                "INNER JOIN authors_books ON authors_books.id_books=books.id_books " +
                "WHERE authors_books.id_authors=" + authorID;

        Connection connection = ConnectionConfiguration.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        ResultSet resultSet = preparedStatement.executeQuery();
        StringBuilder sb = new StringBuilder();

        sb.append("Książki:\n");
        int linesCounter = 0;
        while(resultSet.next()) {
            sb.append(resultSet.getString("books.title")).append(" ")
                    .append("\n");
            ++linesCounter;
        }

        if (linesCounter == 0){
            sb.setLength(0);
            sb.append("Książki:\nBrak książek\n");
        }

        return sb;
    }

    public static Boolean checkIfAuthorExist(String name, String surname, String dateOfBirth, String country){
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = ConnectionConfiguration.getConnection();
            String query = "SELECT * FROM authors WHERE name = ? AND last_name = ? AND date_of_birth = ? AND country = ? ";
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, name);
            preparedStatement.setString(2, surname);
            preparedStatement.setString(3, dateOfBirth);
            preparedStatement.setString(4, country);
            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

    public static Integer checkAuthorId(String name, String surname, String dateOfBirth, String country){
        String query = "SELECT id_authors FROM authors WHERE name = ? AND last_name = ? AND date_of_birth = ? AND country = ? ";
        ResultSet rs = null;
        Connection connection = null;
        PreparedStatement stmt = null;
        try {
            connection = ConnectionConfiguration.getConnection();
            stmt = connection.prepareStatement(query);
            stmt.setString(1, name);
            stmt.setString(2, surname);
            stmt.setString(3, dateOfBirth);
            stmt.setString(4, country);
            rs = stmt.executeQuery();

            while(rs.next()){
                return rs.getInt(1);
            }
        } catch(SQLException se) {
            se.printStackTrace();
        }finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return 1;
    }

    public static void insertAuthorBooks(CAuthorsBooks authorBooks) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;

            try {
                connection = ConnectionConfiguration.getConnection();
                preparedStatement = connection.prepareStatement("INSERT INTO authors_books" +
                        "(id_authors_books, id_authors, id_books, spinner_count)" +
                        "VALUES (?, ?, ?, ?)");
                preparedStatement.setInt(1, authorBooks.getIdAuthorsBooks());
                preparedStatement.setInt(2, authorBooks.getIdAuthors());
                preparedStatement.setInt(3, authorBooks.getIdBooks());
                preparedStatement.setInt(4, authorBooks.getSpinnerCount());

                preparedStatement.executeUpdate();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (preparedStatement != null) {
                    try {
                        preparedStatement.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
                if (connection != null) {
                    try {
                        connection.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        }


        public static Boolean searchAuthorsBooks(Integer authorID, Integer bookID) {
                String query = "SELECT * FROM authors_books WHERE id_authors = ? AND id_books = ?";
            ResultSet rs = null;
            Connection connection = null;
            PreparedStatement stmt = null;
            try {
                connection = ConnectionConfiguration.getConnection();
                stmt = connection.prepareStatement(query);
                stmt.setInt(1, authorID);
                stmt.setInt(2, bookID);
                rs = stmt.executeQuery();

                while (rs.next()) {
                    return true;
                }

            } catch(SQLException se) {
                se.printStackTrace();
            }finally {
                if (rs != null) {
                    try {
                        rs.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
                if (stmt != null) {
                    try {
                        stmt.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
                if (connection != null) {
                    try {
                        connection.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
            return false;

        }


    public static StringBuilder selectAuthorsForBook(Integer bookId) throws SQLException{
        String query = "SELECT DISTINCT authors.name, authors.last_name, authors.date_of_birth, authors.country " +
                "FROM authors " +
                "INNER JOIN authors_books ON authors_books.id_authors=authors.id_authors " +
                "WHERE authors_books.id_books=" + bookId;
        Connection connection = ConnectionConfiguration.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        ResultSet resultSet = preparedStatement.executeQuery();
        StringBuilder sb = new StringBuilder();

        sb.append("Autorzy:\n");
        int linesCounter = 0;
        while(resultSet.next()) {
            sb.append(resultSet.getString("authors.name")).append(" ")
                    .append(resultSet.getString("authors.last_name")).append(", ")
                    .append("data urodzenia: ").append(resultSet.getString("authors.date_of_birth")).append(", ")
                    .append("kraj urodzenia: ").append(resultSet.getString("authors.country"))
                    .append("\n");
            ++linesCounter;
        }

        if (linesCounter == 0){
            sb.setLength(0);
            sb.append("Autorzy:\nBrak autorów\n");
        }

        return sb;
    }

    public static Integer setNewSpinnerCounter(int bookId) throws SQLException {
        String query = "SELECT MAX(spinner_count) " +
                "FROM authors_books " +
                "WHERE id_books=" + bookId;
        Connection connection = ConnectionConfiguration.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        ResultSet resultSet = preparedStatement.executeQuery();
        StringBuilder sb = new StringBuilder();
        while(resultSet.next()){
            sb.append(resultSet.getInt("MAX(spinner_count)"));}
        int lastSpinnerCount = Integer.parseInt(String.valueOf(sb));

        return lastSpinnerCount + 1;
    }

    public static Integer checkLastSpinnerCounter(int bookId) throws SQLException {
        String query = "SELECT MAX(spinner_count) " +
                "FROM authors_books " +
                "WHERE id_books=" + bookId;
        Connection connection = ConnectionConfiguration.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        ResultSet resultSet = preparedStatement.executeQuery();
        StringBuilder sb = new StringBuilder();
        while(resultSet.next()){
            sb.append(resultSet.getInt("MAX(spinner_count)"));}

        return Integer.parseInt(String.valueOf(sb));
    }


}
