package edu.projektJava;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;



public class CMainForm extends JFrame {
    private JTextField bookIdTextFiled;
    private JRadioButton dodajRadioButton;
    private JRadioButton modyfikujRadioButton;
    private JRadioButton usunRadioButton;
    private JButton URUCHOMButton;
    private JRadioButton wyswietlRadioButton;
    private JTextField bookTitleTextField;
    private JTextField bookPublisherTextField;
    private JTextField bookDateTextField;
    private JTextField bookSerieTextField;
    private JTextField bookSerieVolumeTextField;
    private JCheckBox seriaCheckBox;
    private JTextField bookPriceTextField;

    private JTextField authorNameTextField;
    private JTextField authorForenameTextField;
    private JTextField authorBirthdateTextField;
    private JButton zapiszAutoraButton;
    private JSpinner authorSpinner;
    private JTextField authorCountryTextField;
    private JTextField searchBookTitleTextField;
    private JTextField searchBookSerieTextField;
    private JTextField searchBookPublisherTextField;
    private JTextField searchBookSerieVolumeTextField;
    private JTextField searchAuthorCountryTextField;
    private JTextField searchAuthorNameTextField;
    private JTextField searchMinBookPriceTextField;
    private JTextField searchMaxBookPriceTextField;
    private JTextField searchMinAuthorBirthdateTextField;
    private JTextField searchMaxAuthorBirthdateTextField;
    private JTextField searchMinBookDateTextField;
    private JTextField searchMaxBookDateTextField;
    private JTextField codeISBNTextField;
    private JTextField searchCodeIsbn;
    private JPanel MainPanel;
    private JButton SZUKAJButton;
    private JButton RAPORTButton;
    private JTextArea textArea1;
    private JButton WYSWIETLwSZYSTKIEButton;
    private JTextField searchAuthorSurnameTextField;

    public CMainForm(String title) throws HeadlessException {
        super(title);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setContentPane(MainPanel);
        this.pack();
        this.setLocationRelativeTo(null);

        int spinnerID = 1;
        authorSpinner.setValue(spinnerID);

        URUCHOMButton.addActionListener(e -> {
            try {
                button1Click();
            } catch (SQLException ex) {
                throw new RuntimeException(ex);
            }
        });
        zapiszAutoraButton.addActionListener(e -> {
            try {
                button2Click();
            } catch (SQLException ex) {
                throw new RuntimeException(ex);
            }
        });
        SZUKAJButton.addActionListener(e -> {
            try {
                button4Click();
            } catch (SQLException ex) {
                throw new RuntimeException(ex);
            }
        });
        RAPORTButton.addActionListener(e -> button5Click());

        WYSWIETLwSZYSTKIEButton.addActionListener(e -> {
            try {
                button6Click();
            } catch (SQLException ex) {
                throw new RuntimeException(ex);
            }
        });
    }

    private void button1Click() throws SQLException {
        int ID;
        try{
            ID = Integer.parseInt(
                    bookIdTextFiled.getText());
        } catch (NumberFormatException ee){
            JOptionPane.showMessageDialog(
                    this, "Błędnie podane ID książki.");
            return;
        }

        int spinnerID = CLibrary.setNewSpinnerCounter(ID);
        if (spinnerID == 1){
            authorSpinner.setValue(1);
        }else{
            authorSpinner.setValue(spinnerID);
            authorBirthdateTextField.setText("");
            authorCountryTextField.setText("");
            authorForenameTextField.setText("");
            authorNameTextField.setText("");
        }

        if(wyswietlRadioButton.isSelected()){
            String id = bookIdTextFiled.getText();
            Boolean result = CLibrary.getId(id);
            if(!result){
                JOptionPane.showMessageDialog(
                        this, "Książka o podanym id nie istnieje.");
            }else{
                CBooks book = CLibrary.selectBookById(Integer.parseInt(id));
                textArea1.setText("");
                textArea1.append("Dane książki o ID " + ID + ": " + book.getBookInfo());
            }
        }else if(dodajRadioButton.isSelected()) {
            String id = bookIdTextFiled.getText();
            Boolean result = CLibrary.getId(id);
            if(result){
                JOptionPane.showMessageDialog(
                        this, "Książka o takim id już istnieje.");
            }else {

                int id_books = Integer.parseInt(id);
                String title = bookTitleTextField.getText();
                String publisher = bookPublisherTextField.getText();
                String dateOfPublish = bookDateTextField.getText();
                try {
                    LocalDate date = LocalDate.parse(dateOfPublish);
                    String code = codeISBNTextField.getText();
                    if (code.length() != 13 && code.length() != 10) {
                        JOptionPane.showMessageDialog(
                                this, "Kod ISBN niepoprawny. Powinien składać się z 10 lub 13 znaków.");
                    } else {
                        if (!code.matches("(.*)[0-9]-(.*)")) {
                            JOptionPane.showMessageDialog(
                                    this, "Kod ISBN niepoprawny. Powinien składać się z 10 lub 13 cyfr i znaków '-'.");
                        } else {
                            double d = Double.parseDouble(bookPriceTextField.getText());
                            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                            String formattedDate = date.format(formatter);

                            if (dateOfPublish.equals(formattedDate)) {
                                if (!seriaCheckBox.isSelected()) {
                                    CBooks book = new CBooks(id_books, title, publisher, dateOfPublish, code, d);
                                    CLibrary.insertBook(book);
                                    textArea1.setText("");
                                    textArea1.append("Książka o ID " + ID + " została dodana do bazy.");
                                } else {
                                    String seria = bookSerieTextField.getText();
                                    String volume = bookSerieVolumeTextField.getText();
                                    if(volume.matches("[0-9]+")){
                                        int volumeOfSerie = Integer.parseInt(volume);
                                        CBooks book = new CBooks(id_books, title, publisher, dateOfPublish, code, d, seria, volumeOfSerie);
                                        CLibrary.insertBookWithSerie(book);
                                        textArea1.setText("");
                                        textArea1.append("Książka o ID " + ID + " została dodana do bazy.");
                                    }else{
                                        JOptionPane.showMessageDialog(
                                                this, "Niepoprawnie podany tom serii.");
                                    }
                                }
                            } else {
                                JOptionPane.showMessageDialog(
                                        this, "Niepoprawnie podana data. Date podaj w formacie: YYYY-MM-DD");
                            }
                        }
                    }
                } catch (NumberFormatException e) {
                    JOptionPane.showMessageDialog(
                            this, "Niepoprawnie podana cena. Cenę podaj w formacie: 00.00");
                } catch (DateTimeParseException e) {
                    JOptionPane.showMessageDialog(
                            this, "Niepoprawnie podana data. Date podaj w formacie: YYYY-MM-DD");
                }
            }

        }else if(modyfikujRadioButton.isSelected()){
            String id = bookIdTextFiled.getText();
            Boolean result = CLibrary.getId(id);
            if(!result){
                JOptionPane.showMessageDialog(
                        this, "Książka o podanym id nie istnieje.");
            }else{
                CBooks book = CLibrary.selectBookById(ID);
                textArea1.setText("");
                textArea1.append("Książka o ID " + ID + " przed modyfikacją:" + book.getBookInfo() + "\n");

                boolean isTextFieldChanged = false;
                if(!bookTitleTextField.getText().isEmpty()) {
                    book.setTitle(bookTitleTextField.getText());
                    isTextFieldChanged = true;
                }
                if(!bookPublisherTextField.getText().isEmpty()) {
                    isTextFieldChanged = true;
                    book.setPublishingHouse(bookPublisherTextField.getText());
                }
                if(!bookDateTextField.getText().isEmpty()) {

                    try{
                        isTextFieldChanged = true;
                        String date = bookDateTextField.getText();
                        book.setDateOfPublishing(date);
                        LocalDate localDate = LocalDate.parse(date);
                        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                        String formattedDate = localDate.format(formatter);
                    }catch (DateTimeParseException e) {
                        JOptionPane.showMessageDialog(
                                this, "Niepoprawnie podana data. Date podaj w formacie: YYYY-MM-DD");
                    }
                }
                if(!codeISBNTextField.getText().isEmpty()){
                    isTextFieldChanged = true;
                    String code = codeISBNTextField.getText();
                    if (code.length() != 13 && code.length() != 10) {
                        JOptionPane.showMessageDialog(
                                this, "Kod ISBN niepoprawny. Powinien składać się z 10 lub 13 cyfr.");
                    }else if (!code.matches("(.*)[0-9]-(.*)")) {
                        JOptionPane.showMessageDialog(
                                this, "Kod ISBN niepoprawny. Powinien składać się z 10 lub 13 cyfr i znaków '-'.");
                    }else{
                        book.setIsbn(code);
                    }}
                if(!bookPriceTextField.getText().isEmpty()) {
                    try {
                        isTextFieldChanged = true;
                        book.setPrice(Double.parseDouble(bookPriceTextField.getText()));
                    }catch(NumberFormatException e) {
                        JOptionPane.showMessageDialog(
                                this, "Niepoprawnie podana cena. Cenę podaj w formacie: 00.00");
                    }
                }
                if(!bookSerieTextField.getText().isEmpty()) {
                    isTextFieldChanged = true;
                    book.setSerie(bookSerieTextField.getText());
                }
                if(!bookSerieVolumeTextField.getText().isEmpty()) {

                    String volume = bookSerieVolumeTextField.getText();
                    if(volume.matches("[0-9]+")){
                        int volumeOfSerie = Integer.parseInt(volume);
                        book.setSerieVolume(volumeOfSerie);
                        isTextFieldChanged = true;
                    }else{
                        JOptionPane.showMessageDialog(
                                this, "Niepoprawnie podany tom serii.");
                    }
                }

                if(isTextFieldChanged){
                    Boolean changed = CLibrary.updateBook(book, ID);
                    if(changed){
                        textArea1.append("\nDane książki z ID " + ID + " po modyfikacji:\n");
                        textArea1.append("Zapisane dane:");
                        textArea1.append(book.getBookInfo());

                    }

                }
            }

        }else if(usunRadioButton.isSelected()){
            String id = bookIdTextFiled.getText();
            Boolean result = CLibrary.getId(id);
            if(!result){
                JOptionPane.showMessageDialog(
                        this, "Książka o podanym id nie istnieje.");
            }else{
                Boolean deleted = CLibrary.deleteBook(ID);
                if(deleted){
                    textArea1.setText("");
                    textArea1.append("Książka o ID: " + ID + " została usunięta.");
                }
            }
        }
    }

    private void button2Click() throws SQLException {


        int bookId = Integer.parseInt(bookIdTextFiled.getText());
        int spinnerValue = (Integer) authorSpinner.getValue();
        CBooks book = CLibrary.selectBookById(bookId);
        String nameAuthor = authorNameTextField.getText();
        String surnameAuthor = authorForenameTextField.getText();
        String birthdateAuthor = authorBirthdateTextField.getText();
        String countryAuthor = authorCountryTextField.getText();
        if(nameAuthor.isEmpty()) {
            JOptionPane.showMessageDialog(
                    this, "Podaj imię autora. Pole nie może być puste."
            );
        } else if (surnameAuthor.isEmpty()) {
            JOptionPane.showMessageDialog(
                    this, "Podaj nazwisko autora. Pole nie może być puste."
            );
        }else if(birthdateAuthor.isEmpty()){
            JOptionPane.showMessageDialog(
                    this, "Podaj datę urodzenia autora. Pole nie może być puste."
            );
        } else if (countryAuthor.isEmpty()) {
            JOptionPane.showMessageDialog(
                    this, "Podaj kraj urodzenia autora. Pole nie może być puste."
            );

        } else {
            try {
                LocalDate localDate = LocalDate.parse(birthdateAuthor);
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                String formattedDate = localDate.format(formatter);
                int lastSpinner = CLibrary.checkLastSpinnerCounter(bookId);
                if (spinnerValue - 1 != lastSpinner) {
                    JOptionPane.showMessageDialog(
                            this, "Podany autor nie jest " + spinnerValue + " autorem z kolejności dla książki. Poprawny numer autora dla danej książki to " + (lastSpinner + 1)
                    );

                } else {
                    Boolean resultOfCheckAuthor = CLibrary.checkIfAuthorExist(nameAuthor, surnameAuthor, birthdateAuthor, countryAuthor);
                    if (resultOfCheckAuthor) {
                        Integer searchedAuthorId = CLibrary.checkAuthorId(nameAuthor, surnameAuthor, birthdateAuthor, countryAuthor);

                        if (CLibrary.searchAuthorsBooks(searchedAuthorId, bookId)) {
                            JOptionPane.showMessageDialog(
                                    this, "Podany autor został już przypisany do ksiązki."
                            );

                        } else {
                            textArea1.setText("");
                            textArea1.append("Książka przed dodaniem autora:\n");
                            textArea1.append(book.getBookInfo());
                            int authorBooksId = CLibrary.getLastId("authors_books", "id_authors_books");
                            CAuthorsBooks authorBooks = new CAuthorsBooks(authorBooksId, searchedAuthorId, bookId, spinnerValue);
                            CLibrary.insertAuthorBooks(authorBooks);
                            JOptionPane.showMessageDialog(
                                    this, "Podany autor już istnieje.\n Autor został przypisany do książki."
                            );
                            textArea1.append("\nKsiążka po dodaniu autora:\n");
                            textArea1.append(book.getBookInfo());

                        }


                    } else {
                        textArea1.setText("");
                        textArea1.append("Książka przed dodaniem autora:\n");
                        textArea1.append(book.getBookInfo());

                        int authorId = CLibrary.getLastId("authors", "id_authors");
                        CAuthors authorAuthor = new CAuthors(authorId, nameAuthor, surnameAuthor, birthdateAuthor, countryAuthor);
                        CLibrary.insertAuthor(authorAuthor);

                        int authorBooksId = CLibrary.getLastId("authors_books", "id_authors_books");
                        CAuthorsBooks authorBooks = new CAuthorsBooks(authorBooksId, authorId, bookId, spinnerValue);
                        CLibrary.insertAuthorBooks(authorBooks);

                        JOptionPane.showMessageDialog(
                                this, "Podany autor został przypisany do książki."
                        );
                        textArea1.append("\nKsiążka po dodaniu autora:\n");
                        textArea1.append(book.getBookInfo());

                    }
                }

            } catch (DateTimeParseException e) {
                JOptionPane.showMessageDialog(
                        this, "Niepoprawnie podana data. Date podaj w formacie: YYYY-MM-DD");
            }

        }


    }

    private void button4Click() throws SQLException {
        String title = searchBookTitleTextField.getText();
        String publishingHouse = searchBookPublisherTextField.getText();
        String series = searchBookSerieTextField.getText();
        String minDateBook = searchMinBookDateTextField.getText();
        String maxDateBook = searchMaxBookDateTextField.getText();
        String dateMinAuthor = searchMinAuthorBirthdateTextField.getText();
        String dateMaxAuthor = searchMaxAuthorBirthdateTextField.getText();
        String code = searchCodeIsbn.getText();
        String minPrice = searchMinBookPriceTextField.getText();
        String maxPrice = searchMaxBookPriceTextField.getText();
        String serieVolume = searchBookSerieVolumeTextField.getText();
        String authorName = searchAuthorNameTextField.getText();
        String authorSurname = searchAuthorSurnameTextField.getText();
        String countryAuthor = searchAuthorCountryTextField.getText();
        CLibrary cLibrary = new CLibrary();
        StringBuilder sb = new StringBuilder();
        if (authorName.isEmpty() && authorSurname.isEmpty() && dateMaxAuthor.isEmpty() && dateMinAuthor.isEmpty() && countryAuthor.isEmpty()) {
            ResultSet resultSet2 = cLibrary.searchBooks(title, publishingHouse, series, minDateBook, maxDateBook, code, minPrice, maxPrice,  serieVolume);
            sb.append("Wyniki wyszukiwania:\n");
            while (resultSet2.next()) {
                sb.append("---------------------------------------------------------------").append("\n");
                sb.append("ID: ").append(resultSet2.getString("id_books")).append("\n");
                sb.append("Tytuł: ").append(resultSet2.getString("title")).append("\n");
                sb.append("Wydawnictwo: ").append(resultSet2.getString("publishing_house")).append("\n");
                sb.append("Data publikacji: ").append(resultSet2.getString("date_of_publishing")).append("\n");
                sb.append("Kod ISBN: ").append(resultSet2.getString("isbn")).append("\n");
                sb.append("Cena: ").append(resultSet2.getString("price")).append("\n");
                if (resultSet2.getInt("serie_volume") != 0) {
                    sb.append("Nazwa serii: ").append(resultSet2.getString("serie")).append("\n");
                    sb.append("Tom serii: ").append(resultSet2.getString("serie_volume")).append("\n");
                }
                sb.append(CLibrary.selectAuthorsForBook(resultSet2.getInt("id_books")));
                textArea1.setText(sb.toString());
                if(sb.isEmpty()){
                    textArea1.setText("Brak wyników.");
                                  }
                              }
        } else {
            ResultSet resultSet = cLibrary.searchBooks(title, publishingHouse, series, minDateBook, maxDateBook, code, minPrice, maxPrice, serieVolume);
            ResultSet resultSet1 = cLibrary.searchAuthors(authorName, authorSurname, dateMinAuthor, dateMaxAuthor, countryAuthor);
            sb.append("Wyniki wyszukiwania:\n");
            while (resultSet.next()) {
                sb.append("---------------------------------------------------------------").append("\n");
                sb.append("ID: ").append(resultSet.getString("id_books")).append("\n");
                sb.append("Tytuł: ").append(resultSet.getString("title")).append("\n");
                sb.append("Wydawnictwo: ").append(resultSet.getString("publishing_house")).append("\n");
                sb.append("Data publikacji: ").append(resultSet.getString("date_of_publishing")).append("\n");
                sb.append("Kod ISBN: ").append(resultSet.getString("isbn")).append("\n");
                sb.append("Cena: ").append(resultSet.getString("price")).append("\n");
                if (resultSet.getInt("serie_volume") != 0) {
                    sb.append("Nazwa serii: ").append(resultSet.getString("serie")).append("\n");
                    sb.append("Tom serii: ").append(resultSet.getString("serie_volume")).append("\n");
                }
                sb.append(CLibrary.selectAuthorsForBook(resultSet.getInt("id_books")));
            }

            while (resultSet1.next()) {
                sb.append("---------------------------------------------------------------").append("\n");
                sb.append("ID: ").append(resultSet1.getString("id_authors")).append("\n");
                sb.append("Imię: ").append(resultSet1.getString("name")).append("\n");
                sb.append("Nazwisko: ").append(resultSet1.getString("last_name")).append("\n");
                sb.append("Data urodzenia: ").append(resultSet1.getString("date_of_birth")).append("\n");
                sb.append("Miejsce urodzenia: ").append(resultSet1.getString("country")).append("\n");
                sb.append(CLibrary.selectBooksForAuthors(resultSet1.getInt("id_authors")));
                              }

                              textArea1.setText(sb.toString());
                              if(sb.isEmpty()){
                                  textArea1.setText("Brak wyników");
                              }
                          }

    }

    private void button5Click(){
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogType(JFileChooser.SAVE_DIALOG);
        fileChooser.setDialogTitle("Nazwa pliku raportu");
        fileChooser.setFileFilter(
                new FileNameExtensionFilter("Plik tekstowy", "txt"));
        int answer = fileChooser.showSaveDialog(null);
        if (answer == JFileChooser.APPROVE_OPTION) {
            String fname = fileChooser.getSelectedFile().toString();
            if(!fname.endsWith(".txt")) fname += ".txt";
            try(BufferedWriter bw = new BufferedWriter(new FileWriter(fname))){
                textArea1.write(bw);
                JOptionPane.showMessageDialog(this,
                        "Zapisano plik raportu:\n" +fname);
            }catch (IOException e){
                JOptionPane.showMessageDialog(this,
                        "Nieudany zapis pliku raportu:\n"+fname);
            }
        }
    }

    private void button6Click() throws SQLException {
        ResultSet resultSet = CLibrary.selectAllBooks();
        StringBuilder sb = new StringBuilder();
        sb.append("Wszystkie książki:\n");
        while(resultSet.next()){
            sb.append("---------------------------------------------------------------").append("\n");
            sb.append("ID: ").append(resultSet.getString("id_books")).append("\n");
            sb.append("Tytuł: ").append(resultSet.getString("title")).append("\n");
            sb.append("Wydawnictwo: ").append(resultSet.getString("publishing_house")).append("\n");
            sb.append("Data publikacji: ").append(resultSet.getString("date_of_publishing")).append("\n");
            sb.append("Kod ISBN: ").append(resultSet.getString("isbn")).append("\n");
            sb.append("Cena: ").append(resultSet.getString("price")).append("\n");
            if (resultSet.getInt("serie_volume") != 0){
                sb.append("Nazwa serii: ").append(resultSet.getString("serie")).append("\n");
                sb.append("Tom serii: ").append(resultSet.getString("serie_volume")).append("\n");
            }
            sb.append(CLibrary.selectAuthorsForBook(resultSet.getInt("id_books")));
        }
        sb.append("---------------------------------------------------------------").append("\n");
        textArea1.setText(sb.toString());
    }
}
