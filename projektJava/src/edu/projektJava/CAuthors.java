package edu.projektJava;

public class CAuthors {
    private  int idAuthors;
    public  String name;
    public  String lastName;
    public  String dateOfBirth;
    public String country;

    public int getIdAuthors() {return idAuthors;}
    public void setIdAuthors(int idAuthors) {this.idAuthors = idAuthors;}
    public String getName() {return name;}
    public void setName(String name) {this.name = name;}
    public String getLastName() {return lastName;}
    public void setLastName(String lastName) {this.lastName = lastName;}
    public String getDateOfBirth() {return dateOfBirth;}
    public void setDateOfBirth(String dateOfBirth) {this.dateOfBirth = dateOfBirth;}
    public String getCountry() {return country;}
    public void setCountry(String country) {this.country = country;}

    public CAuthors(int idAuthors, String name, String lastName, String dateOfBirth, String country) {
        this.idAuthors = idAuthors;
        this.name = name;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
        this.country = country;
    }

    public CAuthors(String name, String lastName, String dateOfBirth, String country) {
        this.name = name;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
        this.country = country;
    }

    public CAuthors() {
    }

    public String toString(){
        return "AUTOR:\n" + name + " " + lastName +
                "\nNumer w bazie: " + idAuthors +
                "\nMiejsce oraz data urodzenia: " + dateOfBirth + ", " + country;
    }
}