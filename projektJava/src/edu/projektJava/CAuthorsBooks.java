package edu.projektJava;

public class CAuthorsBooks {
    private  int idAuthors;
    private int idAuthorsBooks;
    private  int idBooks;
    private int spinnerCount;

    public int getIdAuthors() {return idAuthors;}
    public void setIdAuthors(int idAuthors) {this.idAuthors = idAuthors;}
    public int getIdAuthorsBooks() {return idAuthorsBooks;}
    public void setIdAuthorsBooks(int idAuthorsBooks) {this.idAuthorsBooks = idAuthorsBooks;}
    public int getIdBooks() {return idBooks;}
    public void setIdBooks(int idBooks) {this.idBooks = idBooks;}
    public CAuthorsBooks() {}
    public int getSpinnerCount() {return spinnerCount;}

    public void setSpinnerCount(int spinnerCount) {
        this.spinnerCount = spinnerCount;
    }

    public CAuthorsBooks(int idAuthorsBooks, int idAuthors, int idBooks, int spinnerCount) {
        this.idAuthors = idAuthors;
        this.idAuthorsBooks = idAuthorsBooks;
        this.idBooks = idBooks;
        this.spinnerCount = spinnerCount;
    }
}

