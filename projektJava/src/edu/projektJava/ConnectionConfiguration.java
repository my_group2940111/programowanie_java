package edu.projektJava;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class ConnectionConfiguration {
    public static Connection getConnection() {
        Connection connection = null;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
          connection = DriverManager.getConnection("");
        }  catch (SQLException | ClassNotFoundException e) {
            System.err.println("Problem z otwarciem polaczenia");
            e.printStackTrace();
        }
        return connection;
    }
}