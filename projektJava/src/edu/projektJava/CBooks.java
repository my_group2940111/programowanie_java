package edu.projektJava;


import java.sql.SQLException;

public class CBooks {

    public  int id_books;

    public String title;
    public  String publishingHouse;
    public  String dateOfPublishing;
    public  String isbn;

    public double price;
    public String serie;
    public  int serieVolume;

    public  int getId_books() {
        return id_books;
    }

    public void setId_books(int id_books){this.id_books = id_books;}

    public String getTitle() {return title;}
    public void setTitle(String title) {this.title = title;}
    public  String getPublishingHouse() {return publishingHouse;}
    public void setPublishingHouse(String publishingHouse) {this.publishingHouse = publishingHouse;}
    public  String getDateOfPublishing() {return dateOfPublishing;}
    public void setDateOfPublishing(String dateOfPublishing) {this.dateOfPublishing = dateOfPublishing;}
    public  String getIsbn() {return isbn;}
    public void setIsbn(String isbn) {this.isbn = isbn;}
    public  String getSerie() {return serie;}
    public void setSerie(String serie) {this.serie = serie;}
    public  int getSerieVolume() {return serieVolume;}
    public void setSerieVolume(int serieVolume) {this.serieVolume = serieVolume;}

    public  double getPrice() {
        return price;
    }
    public void setPrice(double price) {
        this.price = price;
    }

    public CBooks(){

    }
    public CBooks(int id_books, String title, String publishingHouse, String dateOfPublishing, String isbn, double price) {
        this.id_books = id_books;
        this.title = title;
        this.publishingHouse = publishingHouse;
        this.dateOfPublishing = dateOfPublishing;
        this.isbn = isbn;
        this.price = price;

    }

    public CBooks(int id_books, String title, String publishingHouse, String dateOfPublishing, String isbn, double price, String serie, int serieVolume) {
        this.id_books = id_books;
        this.title = title;
        this.publishingHouse = publishingHouse;
        this.dateOfPublishing = dateOfPublishing;
        this.isbn = isbn;
        this.price = price;
        this.serie = serie;
        this.serieVolume = serieVolume;
    }

    public String toString(CBooks cBooks) throws SQLException {
        if (cBooks.serieVolume == 0){
            return
                    "\nID: " + cBooks.id_books +
                    "\nTytuł: "+ cBooks.title +
                    "\nWydawnictwo: "+ cBooks.publishingHouse +
                    "\nData wydania: "+ cBooks.dateOfPublishing +
                    "\nISBN: " + cBooks.isbn +
                    "\nKoszt zakupu: " + cBooks.price +
                    "\n" + CLibrary.selectAuthorsForBook(cBooks.id_books);
        } else {
            return
                    "\nID: " + cBooks.id_books +
                    "\nTytuł: "+ cBooks.title +
                    "\nWydawnictwo: "+ cBooks.publishingHouse +
                    "\nData wydania: "+ cBooks.dateOfPublishing +
                    "\nISBN: " + cBooks.isbn +
                    "\nKoszt zakupu: " + cBooks.price +
                    "\nSeria: " + cBooks.serie + ", tom: " + cBooks.serieVolume+
                            "\n" + CLibrary.selectAuthorsForBook(cBooks.id_books);
        }
    }

    public String getBookInfo() throws SQLException {

        if (serieVolume == 0 ){
            return
                    "\nTytuł: "+ title +
                    "\nWydawnictwo: "+ publishingHouse +
                    "\nData wydania: "+ dateOfPublishing +
                    "\nISBN: " + isbn +
                    "\nKoszt zakupu: " + price+
                            "\n" + CLibrary.selectAuthorsForBook(id_books);
        } else {
            return
                    "\nTytuł: "+ title +
                    "\nWydawnictwo: "+ publishingHouse +
                    "\nData wydania: "+ dateOfPublishing +
                    "\nISBN: " + isbn +
                    "\nKoszt zakupu: " + price +
                    "\nSeria: " + serie + ", tom: " + serieVolume+
                            "\n" + CLibrary.selectAuthorsForBook(id_books);
        }
    }


}
